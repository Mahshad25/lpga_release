# ======================================================================
# Training DATA CONFIGURATION
# ======================================================================

# Training Dataset
TRAINING_DATASET_NAME = ds_infty2_2Dhist_training
TRAINING_DATASET_PATH = ../Data/infty/InftyCDB-2/Training.txt

# Testing Dataset
TESTING_DATASET_NAME = ds_infty2_2Dhist_testing
TESTING_DATASET_PATH = ../Data/infty/InftyCDB-2/small_Testing.txt


EXPERT_TYPE = 3
DEFAULT_NUMBER_OF_WORKERS = 8

# ======================================================================
# CNN INPUT  CONFIGURATION
# ======================================================================

CNN_IMAGE_SIZE = 224
CNN_IMAGE_OUTPUT_DIR = ../output/CNN_img_test/ 
CNN_IMAGE_LABEL_DIR =  ../output/CNN_img_test/testIMG_labels.txt

# ======================================================================
# DATA PROCESSING CONFIGURATION
# ======================================================================

# Data loader
# INKML = load stroke data from .inkml files
# IMG = load image data from .PNG and .lg files
INPUT_DATA_LOADER_TYPE = IMG

GRAPH_LEVEL = symbol

SAVE_EXPRESSION_CONTEXT = 1

## Image Processing Parameters

IMAGE_PROCESSING_LG_PATH = ../Data/Expressions/LG/
IMAGE_PROCESSING_IMG_PATH = ../Data/Expressions/IMG/


# Trace Type
#   Stroke: traces made to resemble handwritten strokes.
#   Contour: Uses the contours of symbol as traces.
IMAGE_PROCESSING_TRACE_TYPE = Contour

IMAGE_PROCESSING_CONTOUR_REDUCTION_TYPE = Smooth
IMAGE_PROCESSING_CONTOUR_REDUCTION_VALUE = 1
IMAGE_PROCESSING_CONTOUR_REDUCTION_SMOOTHING_DISTANCE = 2

# ======================================================================
# PREPROCESSING CONFIGURATION
# ======================================================================

PREPROCESS_SMOOTHEN = CATMULL

NORMALIZE_TRACE_SETS = 0


# ======================================================================
# GRAPH CONFIGURATION
# ======================================================================

INITIAL_GRAPH_TYPE = sight
FIND_PUNCTUATION = 1
LOS_SYMBOL_SHRINK = 1.0

# =======================================================================
# Feature Extraction Configuration
#========================================================================
PARALLEL_FEATURE_EXTRACTION_WORKERS = 8

# Geometric Features
FEATURES_USE_GEOMETRIC_FEATURES = 1

LEI_HU_FILTER = 0

BOUNDING_BOX_DISTANCES_RATIOS = 1
BOUNDING_BOX_DISTANCES_CENTER_NORMALIZATION = 1
BOUNDING_BOX_OVERLAPS = 1
BOUNDING_BOX_SIZES = 1
BOUNDING_BOX_AREAS = 1
POINT_DISTANCES = 1
START_END_POINT_DISTANCES = 1
MASS_CENTERS_DISTANCES = 1
TRACE_ANGLES = 1

# Shape Context Features for relations
FEATURES_USE_EXPRESSION_SHAPE_CONTEXT_FEATURES = 0
BOUND_SHAPE_CONTEXT_FEATURES = 0

EXPRESSION_SHAPE_CONTEXT_CIRCLES = [[5, 12, 'UNIFORM', 'COUNT_POINTS']]
EXPRESSION_SHAPE_CONTEXT_RADIUS_METHOD = ['SYMBOL_FARTHEST_PT']
EXPRESSION_SHAPE_CONTEXT_RADIUS_FACTOR = [1.5]
EXPRESSION_SHAPE_CONTEXT_USE_GLOBAL_CONTEXT = ['CONTEXT_AND_SYMBOL']

SHAPE_CONTEXT_DEBUG_MODE = 0
SHAPE_CONTEXT_INTENSITY_TYPE = 1
PLOT_PATH = output/visuals/rel_2dh_pt_10/

# Time Order Features
FEATURES_USE_TIME_ORDER_FEATURES = 0

# Grids
# Grid format
# [List of grids]
# Grid = [rows, cols, % width, % height, offset x (%), offset y (%), Use Lenghts (True) or Points (False), Grid Type, Use Parzen (True) for membership, Standard Deviation factor for Parzen ]
# Note that rows, cols represent corners on each direction
# The grid will have (rows - 1) * (cols - 1) cells
# A feature dimension will be added per corner of the grid
# 2D_HISTOGRAMS_GRIDS = [[5, 5, 1.0, 1.0, 0.0, 0.0, True, False, 0.1], [4, 4, 0.8, 0.8, 0.1, 0.1, True, False, 0.1]]

FEATURES_USE_PARENT_2D_HISTOGRAMS = 0
PARENT_2D_HISTOGRAMS_GRIDS = [[5, 5, 1.0, 1.0, 0.0, 0.0, False, 1, False, 0.1]]

FEATURES_USE_CHILD_2D_HISTOGRAMS = 0 #MM: turned off for ectracting geo feat only
CHILD_2D_HISTOGRAMS_GRIDS = [[6, 6, 1.0, 1.0, 0.0, 0.0, False, 1, False, 0.1]]

FEATURES_USE_RELATION_2D_HISTOGRAMS = 0 #MM: turned off for ectracting geo feat only
RELATION_2D_HISTOGRAMS_GRIDS = [[10, 10, 1.0, 1.0, 0.0, 0.0, False, 0, True, 0.1]]

FEATURES_USE_MST_CONTEXT_2D_HISTOGRAMS = 0
MST_CONTEXT_2D_HISTOGRAMS_GRIDS = [[5, 5, 1.0, 1.0, 0.0, 0.0, False, 0, True, 0.1]]


# ======================================================================
# Classifier configuration
# ======================================================================

# Classifier Parameters
# - Classifier type
#   * 1 = Random Forest
#   * 2 = Linear Support Vector Machine
#   * 3 = Radial Basis Function Support Vector Machine
CLASSIFIER_TYPE = 1 # 3?

CLASSIFIER_TRAINING_TIMES = 1

CLASSIFIER_CROSSVALIDATION_PARTITIONS = 5

# Classifier files output directory
CLASSIFIER_DIR = ../output/classifiers
CLASSIFIER_NAME = rf_infty2_2Dhist

RANDOM_FOREST_N_TREES = 50 # 50, 100, 200
RANDOM_FOREST_MAX_DEPTH = 40 # 12, 15, 18
RANDOM_FOREST_MAX_FEATURES = 30 # 10, 30, 50
RANDOM_FOREST_CRITERION = 1 # 0 = ENTROPY, 1 = GINI
RANDOM_FOREST_JOBS = 8

LINEAR_SVM_PROBABILISTIC = 0

RBF_SVM_PROBABILISTIC = 0
RBF_SVM_C = 10.0
RBF_SVM_GAMMA = 0.001

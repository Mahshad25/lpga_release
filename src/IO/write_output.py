import os
import numpy as np
import networkx as nx
from recognizer.graph_representation.Graph_construction import EDGE_ATTRIBUTES


def write_lgfile(eval_data_set, symbolIds):
    currentDir = os.path.dirname(os.getcwd())
    source_data = eval_data_set.sources
    class_map = eval_data_set.label_mapping
    prefix = "O, Obj"
    for source, symbol_id in zip(source_data,symbolIds):
        # find the file name to write the symbol
        file_name = source[0].replace(".inkml", ".lg")
        traces = ', '.join(str(x) for x in source[2])
        symbol = class_map[int(symbol_id)]
        output_line = ", ".join((prefix, symbol, "1.0", traces))+"\n"
        #write information into the file
        output_file = open(file_name, "a+")
        output_file.write(output_line)
        output_file.close()

def write_classified(location,eval_data_set, cls_labels):
    os.makedirs(location, exist_ok=True)
    #sources = np.array(eval_data_set.sources)
    file_ids = [s[0] for s in eval_data_set.sources]
    sym_ids = [s[1] for s in eval_data_set.sources]
    sym_prims = [s[2] for s in eval_data_set.sources]
    class_map = eval_data_set.label_mapping

    prefix = "O, "
    suffix = ", 1.0, "

    for i in range(len(sym_ids)):
        if file_ids[i][-6:].lower() == ".inkml":
            path, file_name = os.path.split(file_ids[i])
            file_name = file_name.replace(".inkml", ".lg")
        else:
            file_name = file_ids[i] + ".lg"
        primataves = ', '.join(str(x) for x in sym_prims[i])
        output_file = open(location + "//" + file_name, "a+")
        symbol = class_map[int(cls_labels[i])]
        output_file.write(prefix + str(sym_ids[i]) + ", " + symbol + suffix + primataves + "\n")
        output_file.close()


def write_stroke_tree_lg_file(layout_tree, file_name):
    #this is the lg writer to write stroke-layout graph
    nodes = layout_tree.nodes()
    edges = layout_tree.edges()
    output_file_name = file_name.replace(".inkml", ".lg")
    output_file = open(output_file_name, "a+")
    node_labels = nx.get_node_attributes(layout_tree, 'predicted_label')
    print(node_labels)

    node_prefix = "N"
    #write the nodes in the lg file
    for node in nodes:
        output_line = ", ".join((node_prefix, str(node), str(node_labels[node]),"1.0")) + "\n"
        output_file.write(output_line)

    #write the relations in lg file
    edge_header = "\n\n# Edges:\n# FORMAT: E, Primitive ID (parent), Primitive ID (child), Label, Weight\n"
    output_file.write(edge_header)
    edge_labels = nx.get_edge_attributes(layout_tree, EDGE_ATTRIBUTES.PREDICTED_LAYOUT)
    edges_prefix = "E"
    for edge in edges:
        traces = ", ".join(str(x) for x in edge)
        ed_label = str(edge_labels[edge])
        if ed_label != "NoRelation":
            output_line = ", ".join((edges_prefix, traces, ed_label, "1.0")) + "\n"
            output_file.write(output_line)

    output_file.close()



def write_symbol_tree_lg_file(layout_tree, file_name):
    #this is the lg writer to write stroke-layout graph
    nodes = layout_tree.nodes()
    edges = layout_tree.edges()
    output_file_name = file_name.replace(".inkml", ".lg")
    output_file = open(output_file_name, "a+")

    node_strokesets = nx.get_node_attributes(layout_tree,'stroke_set')
    node_labels = nx.get_node_attributes(layout_tree, 'predicted_label')
    node_prefix = "O, Obj"
    #write the nodes in the lg file
    for node in nodes:
        str_traces = ', '.join(str(x) for x in node_strokesets[node].trace_list)
        output_line = ", ".join((node_prefix+str(node), node_labels[node], "1.0", str_traces)) + "\n"
        output_file.write(output_line)

    #write the relations in lg file
    edge_header = "\n\n# Edges:\n# FORMAT: E, Primitive ID (parent), Primitive ID (child), Label, Weight\n"
    output_file.write(edge_header)
    edge_labels = nx.get_edge_attributes(layout_tree, EDGE_ATTRIBUTES.PREDICTED_LAYOUT)
    edge_weight = nx.get_edge_attributes(layout_tree, 'weight')
    edges_prefix = "EO"
    for edge in edges:
        two_symbols = "Obj"+str(edge[0])+", "+"Obj"+str(edge[1])
        output_line = ", ".join((edges_prefix, two_symbols, edge_labels[edge], str(edge_weight[edge]))) + "\n"
        output_file.write(output_line)

    output_file.close()
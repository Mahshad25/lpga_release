import math
import numpy as np

from recognizer.util.math_ops import MathOps
from recognizer.symbols.math_symbol_trace import MathSymbolTrace

class CrossingsFeatureExtractor:
    @staticmethod
    def computeTraceLineCrossings(trace, line):
        l_x1, l_y1 = line[0]
        l_x2, l_y2 = line[1]

        if l_x1 == l_x2 and l_y1 == l_y2:
            #it's a point, no crossings with a single point...
            return []

        #check minimum/maximum
        l_xmin, l_xmax  = (l_x1, l_x2) if l_x1 < l_x2 else (l_x2, l_x1)
        l_ymin, l_ymax  = (l_y1, l_y2) if l_y1 < l_y2 else (l_y2, l_y1)

        minX, maxX, minY, maxY = trace.getBoundaries()
        if not (minX < l_xmax and maxX > l_xmin and minY < l_ymax and maxY > l_ymin):
            #not even on the same bounding box ...
            return []

        points = trace.points

        crossings = []
        if l_x1 != l_x2:
            # use the slope and intersect to compare...
            l_m = (l_y2 - l_y1) / (l_x2 - l_x1)
            l_b = l_y1 - l_m * l_x1

            # for every segment....
            for i in range(len(points) - 1):
                s_x1, s_y1 = points[i]
                s_x2, s_y2 = points[i + 1]

                if s_x2 == s_x1:
                    # the segment is a vertical line...
                    if l_xmin <= s_x1 and s_x1 <= l_xmax:
                        # the vertical segment is inside the range of the current line...
                        y_int = s_x1 * l_m + l_b

                        # check if y_int in range of vertical line...
                        if min(s_y1, s_y2) <= y_int and y_int <= max(s_y1, s_y2):
                            # intersection found...
                            crossings.append( (s_x1, y_int) )
                else:
                    # the segment is not a vertical line
                    s_m = (s_y2 - s_y1) / (s_x2 - s_x1)
                    s_b = s_y1 - s_m * s_x1

                    # check if parallel
                    if s_m == l_m:
                        # parallel lines, can only intersect if l_b == s_b
                        # (meaning they are the same line), and have intersecting ranges
                        if l_b == s_b:
                            if l_xmin <= max(s_x1, s_x2) and min(s_x1, s_x2) <= l_xmax:
                                 # intersection found
                                 crossings.append(((s_x1 + s_x2) / 2.0, (s_y1 + s_y2) / 2.0))

                    else:
                        # not parallel, they must have an intersection point
                        x_int = (s_b - l_b) / (l_m - s_m)
                        y_int = x_int * l_m + l_b

                        # the intersection point must be in both lines...
                        if (l_xmin <= x_int <= l_xmax) and (min(s_x1, s_x2) <= x_int <= max(s_x1, s_x2)):
                            crossings.append((x_int, y_int))

        else:
            # the given line is a vertical line...
            # can't use the slope, use a different method
            # for every segment....
            for i in range(len(points) - 1):
                s_x1, s_y1 = points[i]
                s_x2, s_y2 = points[i + 1]

                if s_x2 == s_x1:
                    # the segment is a vertical line (too)...
                    # only if they are on the same x position, and their range intersects
                    if s_x1 == l_x1 and min(s_y1, s_y2) < l_ymax and l_ymin < max(s_y1, s_y2):
                        crossings.append(((s_x1 + s_x2) / 2.0, (s_y1 + s_y2) / 2.0))
                else:
                    # calculate intersection point
                    if min(s_x1, s_x2) <= l_x1 <= max(s_x1, s_x2):
                        # the vertical line is inside the range of the current segment...
                        s_m = (s_y2 - s_y1) / (s_x2 - s_x1)
                        s_b = s_y1 - s_m * s_x1

                        y_int = l_x1 * s_m + s_b
                        # check if y_int in range of vertical line...
                        if l_ymin <= y_int <= l_ymax:
                            # intersection found...
                            crossings.append((l_x1, y_int))

        return crossings

    @staticmethod
    def computeSymbolCrossings(symbol, line_groups):
        all_crossings = {
            "counts" : [],
            "mins" : [],
            "maxs" : [],
        }

        # for each crossing group
        for line_group in line_groups:
            # initial values
            crossing_count = 0.0
            avg_min = 0.0
            avg_max = 0.0

            # for each sub-crossing
            for line in line_group:
                l_x1, l_y1 = line[0]
                l_x2, l_y2 = line[1]
                line_weight = line[2]
                local_line = [line[0], line[1]]

                l_mx = (l_x1 + l_x2) / 2.0
                l_my = (l_y1 + l_y2) / 2.0

                max_dist = MathOps.euclideanDistance(l_x1, l_y1, l_mx, l_my) + 0.1

                current_min = max_dist
                current_max = -max_dist

                # for each trace in symbol
                local_crossing_count = 0
                for trace in symbol.traces:
                    current_crossings = CrossingsFeatureExtractor.computeTraceLineCrossings(trace, local_line)

                    local_crossing_count += float(len(current_crossings))
                    for x, y in current_crossings:
                        # Use mid point as reference, and compute distance to the other two points
                        dist_init = MathOps.euclideanDistance(x, y, l_x1, l_y1)
                        dist_end = MathOps.euclideanDistance(x, y, l_x2, l_y2)
                        dist_mid = MathOps.euclideanDistance(x, y, l_mx, l_my)

                        dist_val = (-1 if dist_init < dist_end else 1) * dist_mid

                        current_min = min(current_min, dist_val)
                        current_max = max(current_max, dist_val)

                # store limits
                crossing_count += local_crossing_count * line_weight
                avg_min += current_min * line_weight
                avg_max += current_max * line_weight

            all_crossings["counts"].append(crossing_count)
            all_crossings["mins"].append(avg_min)
            all_crossings["maxs"].append(avg_max)

        return all_crossings

    @staticmethod
    def getHorizontalVerticalCrossings(symbol, number_crossings, number_subcrossings,
                                       subcrossing_weights, subcrossing_sigma, use_overlapping):
        # ...count how many line segments are crossed by horizontal and vertical lines
        # at different heights and widths (2 by 2 box)
        step = 2.0 / (number_crossings + 1)
        substep = step / (number_subcrossings + 1)
        if subcrossing_weights == 1:
            # gaussian weighting
            weights = MathOps.get_1d_gaussian_kernel(number_subcrossings, subcrossing_sigma)
        else:
            # uniform distribution of weights
            weights = [1.0 / float(number_subcrossings) for i in range(number_subcrossings)]

        if use_overlapping:
            # extra crossings with overlaps
            crossing_regions = number_crossings * 2 - 1
            i_w = 0.5
            i_start = 0.0
        else:
            # normal crossings (no overlaps)
            crossing_regions = number_crossings
            i_w = 1.0
            i_start = 0.5

        # create sub-crossings lines ...
        all_crossings_groups = []
        for i in range(1, crossing_regions + 1):
            horizontal_group = []
            vertical_group = []
            for k in range(1, number_subcrossings + 1):
                init = -1 + (i * i_w - i_start) * step + k * substep

                # horizontal
                line = [(-1.1, init), (1.1, init), weights[k - 1]]
                horizontal_group.append(line)

                # vertical
                line = [(init, -1.1), (init, 1.1), weights[k - 1]]
                vertical_group.append(line)

            all_crossings_groups.append(horizontal_group)
            all_crossings_groups.append(vertical_group)

        # compute crossings ...
        return CrossingsFeatureExtractor.computeSymbolCrossings(symbol, all_crossings_groups)

    @staticmethod
    def getDiagonalCrossings(symbol, number_crossings, number_subcrossings, subcrossing_weights, subcrossing_sigma,
                             use_overlapping):

        diagonal_l = 2.0 * math.sqrt(2.0)

        # ...count how many line segments are crossed by horizontal and vertical lines
        # at different heights and widths (2 by 2 box)
        step = diagonal_l / (number_crossings + 1)
        substep = step / (number_subcrossings + 1)

        if subcrossing_weights == 1:
            # gaussian weighting
            weights = MathOps.get_1d_gaussian_kernel(number_subcrossings, subcrossing_sigma)
        else:
            # uniform distribution of weights
            weights = [1.0 / float(number_subcrossings) for i in range(number_subcrossings)]

        if use_overlapping:
            # extra crossings with overlaps
            crossing_regions = number_crossings * 2 - 1
            i_w = 0.5
            i_start = 0.0
        else:
            # normal crossings (no overlaps)
            crossing_regions = number_crossings
            i_w = 1.0
            i_start = 0.5

        # create sub-crossings lines ...
        all_crossings_groups = []
        # assuming a 2x2 box
        half_diag = math.sqrt(2.0)
        for i in range(1, crossing_regions + 1):
            main_diagonal = []
            other_diagonal = []
            for k in range(1, number_subcrossings + 1):
                # current diagonal length from starting point
                diag_pos = -half_diag + (i * i_w - i_start) * step + k * substep

                # length of each side of the triangle with hypotenuse = diagonal
                diag_side = math.sqrt(((diag_pos + half_diag) ** 2) / 2.0)

                # compute point in main diagonal
                main_x, main_y = -1 + diag_side, -1 + diag_side

                # compute line crossing point in main diagonal
                # (The line is parallel to the other diagonal )
                line = [(main_x - 1, main_y + 1), (main_x + 1, main_y - 1), weights[k - 1]]
                main_diagonal.append(line)

                # compute point in other diagonal
                other_x, other_y = -1 + diag_side, 1 - diag_side
                # compute line crossing point in other diagonal
                # (The line is parallel to the main diagonal )
                line = [(other_x - 1, other_y - 1), (other_x + 1, other_y + 1), weights[k - 1]]
                other_diagonal.append(line)

            all_crossings_groups.append(main_diagonal)
            all_crossings_groups.append(other_diagonal)

        # compute crossings ...
        return CrossingsFeatureExtractor.computeSymbolCrossings(symbol, all_crossings_groups)

    @staticmethod
    def getAngularCrossings(symbol, number_angular, angular_subcrossings, subcrossing_weights, subcrossing_sigma):
        # use the center of the box ...
        cx = 0.0
        cy = 0.0

        if subcrossing_weights == 1:
            # gaussian weighting
            weights = MathOps.get_1d_gaussian_kernel(angular_subcrossings, subcrossing_sigma)
        else:
            # uniform distribution of weights
            weights = [1.0 / float(angular_subcrossings) for i in range(angular_subcrossings)]

        step = (math.pi * 0.5) / (number_angular + 1)
        substep = step / (angular_subcrossings + 1)

        all_crossing_groups = []

        for i in range(1, number_angular + 1):
            # first r between 0 and 90 degrees (to 180 and 270)
            # second r between 90 and 180 degrees (to 270 and 360)
            for r in range(2):
                current_crossing_group = []
                for k in range(1, angular_subcrossings + 1):
                    # angle = step * i + math.pi * 0.5 * r
                    angle = math.pi * 0.5 * r + (i - 0.5) * step + k * substep

                    init_x = cx + 3 * math.cos(angle)
                    init_y = cy + 3 * math.sin(angle)
                    end_x = cx - 3 * math.cos(angle)
                    end_y = cy - 3 * math.sin(angle)

                    line = [(init_x, init_y), (end_x, end_y), weights[k - 1]]

                    current_crossing_group.append(line)

                all_crossing_groups.append(current_crossing_group)

        # compute crossings ...
        return CrossingsFeatureExtractor.computeSymbolCrossings(symbol, all_crossing_groups)

    @staticmethod
    def getZoningCrossings(symbol, n_hor_lines, n_ver_lines, n_dia_lines, weight_function, weight_sigma, grids):
        # create lines
        all_line_groups = []

        # ... horizontal ...
        hor_lines = []
        step = 2.0 / (n_hor_lines + 1)
        for i in range(1, n_hor_lines + 1):
            init = -1.0 + step * i
            line = [(-1.0, init), (1.0, init)]

            hor_lines.append(line)

        # ... vertical ...
        ver_lines = []
        step = 2.0 / (n_ver_lines + 1)
        for i in range(1, n_ver_lines + 1):
            init = -1.0 + step * i
            line = [(init, -1.0), (init, 1.0)]

            ver_lines.append(line)

        # ... diagonal ...
        main_diagonal_lines = []
        other_diagonal_lines = []
        diagonal_l = 2.0 * math.sqrt(2.0)
        step = diagonal_l / (n_dia_lines + 1)
        half_diag = math.sqrt(2.0)
        for i in range(1, n_dia_lines + 1):
            # current diagonal length from starting point
            diag_pos = -half_diag + i * step

            # length of each side of the triangle with hypotenuse = diagonal
            diag_side = math.sqrt(((diag_pos + half_diag) ** 2) / 2.0)

            # compute point in main diagonal
            main_x, main_y = -1 + diag_side, -1 + diag_side

            # compute line crossing point in main diagonal
            # (The line is parallel to the other diagonal )
            line = [(main_x - 1, main_y + 1), (main_x + 1, main_y - 1)]
            main_diagonal_lines.append(line)

            # compute point in other diagonal
            other_x, other_y = -1 + diag_side, 1 - diag_side
            # compute line crossing point in other diagonal
            # (The line is parallel to the main diagonal )
            line = [(other_x - 1, other_y - 1), (other_x + 1, other_y + 1)]
            other_diagonal_lines.append(line)

        all_line_groups.append(hor_lines)
        all_line_groups.append(ver_lines)
        all_line_groups.append(main_diagonal_lines)
        all_line_groups.append(other_diagonal_lines)

        # compute crossings
        line_crossings = []
        # ... for each group ...
        for line_group in all_line_groups:
            line_group_crossings = []
            # for each line in the group
            for line in line_group:
                # compute crossings with all traces
                current_crossings = []
                for trace in symbol.traces:
                    current_crossings += CrossingsFeatureExtractor.computeTraceLineCrossings(trace, line)

                line_group_crossings.append(current_crossings)

            line_crossings.append(line_group_crossings)

        symbol_features = []
        for n, m, w_prc, h_prc, off_x, off_y in grids:
            start_x = -1.0 + off_x * 2.0
            start_y = -1.0 + off_y * 2.0
            cell_h = (2.0 * h_prc) / n
            cell_w = (2.0 * w_prc) / m

            # evaluate each cell on the grid
            for grid_row in range(n):
                for grid_col in range(m):
                    left_x = start_x + grid_col * cell_w
                    right_x = start_x + (grid_col + 1) * cell_w
                    top_y = start_y + grid_row * cell_h
                    bottom_y = start_y + (grid_row + 1) * cell_h

                    corner_lt = left_x, top_y
                    corner_lb = left_x, bottom_y
                    corner_rt = right_x, top_y
                    corner_rb = right_x, bottom_y

                    box = [corner_lt, corner_rt, corner_rb, corner_lb, corner_lt]
                    box_trace = MathSymbolTrace(1, box)

                    # compute lines intersecting box per group ....
                    tempo_counts = []
                    for g_idx, line_group in enumerate(all_line_groups):
                        per_group_crossings = []

                        # for each line in the group
                        for l_idx, line in enumerate(line_group):
                            intersects = CrossingsFeatureExtractor.computeTraceLineCrossings(box_trace, line)
                            if len(intersects) > 0:
                                # find crossings for this line within the box ...
                                current_crossings = line_crossings[g_idx][l_idx]
                                valid_crossings = 0
                                for x, y in current_crossings:
                                    if left_x <= x <= right_x and top_y <= y <= bottom_y:
                                        # within the box, add
                                        valid_crossings += 1
                                per_group_crossings.append(valid_crossings)

                        # compute weighted count
                        number_subcrossings = len(per_group_crossings)
                        if weight_function == 1:
                            # gaussian weighting
                            weights = MathOps.get_1d_gaussian_kernel(number_subcrossings, weight_sigma)
                        else:
                            # uniform distribution of weights
                            weights = [1.0 / float(number_subcrossings) for i in range(number_subcrossings)]

                        weighted_count = (np.array(weights) * np.array(per_group_crossings)).sum()

                        symbol_features.append(weighted_count)

        return symbol_features
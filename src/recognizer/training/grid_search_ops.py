
class GridSearchOps:
    @staticmethod
    def extract_search_parameters(main_config, search_config):
        # prepare grid search ....
        grid_parameters = []
        grid_parameter_values = {}

        grid_desc = "\n\nGrid Search:\n"
        reserved_parameters = ["REGENERATE_DATASET", "USE_CROSSVALIDATION", "CLASSIFIER_CROSSVALIDATION_PARTITIONS",
                               "GRID_SEARCH_COMBINE"]

        # check first combined parameters (Note this order is assumed by combination generator function)
        grid_combined = []
        if "GRID_SEARCH_COMBINE" in search_config.data:
            combined_parameters_data = search_config.get("GRID_SEARCH_COMBINE")
            assert isinstance(combined_parameters_data, dict)

            for parameter in sorted(combined_parameters_data.keys()):
                grid_combined.append(parameter)
                grid_parameters.append(parameter)

                # check if in main config
                status = "OK" if parameter in main_config.data else "NEW"

                grid_parameter_values[parameter] = combined_parameters_data[parameter]
                grid_desc += (str(len(grid_parameters)) + " - " + parameter + " = " +
                              str(grid_parameter_values[parameter]) + " (COMBINED, " + status + ")\n")

        # validate combinations have the same number of values
        num_combined = None
        for parameter in grid_combined:
            if num_combined is None:
                # number of values of first combined parameter
                num_combined = len(grid_parameter_values[parameter])
            else:
                # pass
                if len(grid_parameter_values[parameter]) != num_combined:
                    raise Exception("Combined parameters must have the same number of values <" + parameter + ">")

        # other non-combined parameters
        for parameter in sorted(search_config.data.keys()):
            value_is_list = search_config.data[parameter][0] == "[" and search_config.data[parameter][-1] == "]"

            if parameter not in reserved_parameters and value_is_list:
                grid_parameters.append(parameter)

                # check if in main config
                status = "OK" if parameter in main_config.data else "NEW"

                grid_parameter_values[parameter] = search_config.get(parameter)
                grid_desc += (str(len(grid_parameters)) + " - " + parameter + " = " +
                              str(grid_parameter_values[parameter]) + " (" + status + ")\n")

        return grid_parameters, grid_parameter_values, grid_combined, grid_desc

    @staticmethod
    def write_search_header(out_append_filename, grid_desc, num_parameters):
        out_file = open(out_append_filename, "a")
        out_file.write(grid_desc)
        out_file.write(str(num_parameters + 1) + " - Training global accuracy \n")
        out_file.write(str(num_parameters + 2) + " - Training average per-class accuracy\n")
        out_file.write(str(num_parameters + 3) + " - Training average per-class stdev\n")
        out_file.write(str(num_parameters + 4) + " - Evaluation global accuracy \n")
        out_file.write(str(num_parameters + 5) + " - Evaluation average per-class accuracy\n")
        out_file.write(str(num_parameters + 6) + " - Evaluation average per-class stdev\n")
        out_file.write(str(num_parameters + 7) + " - Total features\n")
        out_file.write(str(num_parameters + 8) + " - Confusions\n")
        out_file.close()


    @staticmethod
    def enumerate_combinations_rec(grid_parameters, grid_values, pos, current_combination):
        if pos >= len(grid_parameters):
            return [current_combination]
        else:
            children_comb = []
            for value in grid_values[grid_parameters[pos]]:
                comb_copy = list(current_combination)
                comb_copy.append(value)

                children_comb += GridSearchOps.enumerate_combinations_rec(grid_parameters, grid_values, pos + 1, comb_copy)

            return children_comb

    @staticmethod
    def enumerate_combinations(grid_parameters, parameter_values, grid_combined):
        if len(grid_combined) > 0:
            # Note that this function assumes that parameters are listed in the same order in
            # both grid_parameters and grid_combined, and that combined parameters
            # will be at the beginning of the list of grid_parameters
            children_comb = []
            for idx in range(len(parameter_values[grid_combined[0]])):
                # create base combination
                current_combination = []
                for parameter in grid_combined:
                    current_combination.append(parameter_values[parameter][idx])

                # call recursion
                children_comb += GridSearchOps.enumerate_combinations_rec(grid_parameters, parameter_values,
                                                                          len(grid_combined), current_combination)

            return children_comb
        else:
            return GridSearchOps.enumerate_combinations_rec(grid_parameters, parameter_values, 0, [])

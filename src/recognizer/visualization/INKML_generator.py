
class INKMLGenerator:
    Namespace = "http://www.w3.org/2003/InkML"
    @staticmethod
    def save_symbols(path, symbols, annotations=None, mathml=None):
        inkml_header = "<ink xmlns=\"" + INKMLGenerator.Namespace + "\">\n"
        inkml_header += " <traceFormat>\n"
        inkml_header += "  <channel name=\"X\" type=\"decimal\"/>\n"
        inkml_header += "  <channel name=\"Y\" type=\"decimal\"/>\n"
        inkml_header += " </traceFormat>\n"

        inkml_annot = ""
        if annotations is not None:
            for key in annotations:
                inkml_annot += " <annotation type=\"" + key + "\">" + annotations[key] + "</annotation>\n"

        inkml_mathml = ""
        if mathml is not None:
            inkml_mathml += " <annotationXML type=\"truth\" encoding=\"Content-MathML\">\n"
            inkml_mathml += mathml
            inkml_mathml += "</annotationXML>\n"

        inkml_footer = "</ink>"

        # count traces in current group
        total_traces = 0
        for symbol in symbols:
            total_traces += len(symbol.traces)

        # generate traces and trace groups
        inkml_traces = ""
        inkml_symbols = "    <traceGroup xml:id=\"" + str(total_traces) + "\">\n"
        inkml_symbols += "      <annotation type=\"truth\">Segmentation</annotation>\n"
        trace_offset = 0
        symbol_count = {}
        for sym_idx, symbol in enumerate(symbols):

            if symbol.truth in symbol_count:
                symbol_count[symbol.truth] += 1
            else:
                symbol_count[symbol.truth] = 1

            current_symbol_inkml = "      <traceGroup xml:id=\"" + str(total_traces + sym_idx + 1) + "\">\n"
            current_symbol_inkml += "         <annotation type=\"truth\">" + symbol.truth + "</annotation>\n"

            for trace_idx, trace in enumerate(symbol.traces):
                curr_idx = trace_offset + trace_idx
                current_trace = "    <trace id=\"" + str(curr_idx) + "\">\n"
                current_trace += "    " + ",".join([str(x) + " " + str(y) for x, y in trace.points]) + "\n"
                current_trace += "    </trace>\n"

                current_symbol_inkml += "         <traceView traceDataRef=\"" + str(curr_idx) + "\"/>\n"

                inkml_traces += current_trace
            trace_offset += len(symbol.traces)

            current_symbol_inkml += "         <annotationXML href=\"" + symbol.truth + "_" + str(symbol_count[symbol.truth]) + "\"/>\n"
            current_symbol_inkml += "      </traceGroup>\n"
            inkml_symbols += current_symbol_inkml

        inkml_symbols += "    </traceGroup>\n"

        out_file = open(path, "w")
        out_file.write(inkml_header)
        out_file.write(inkml_annot)
        out_file.write(inkml_mathml)
        out_file.write(inkml_traces)
        out_file.write(inkml_symbols)
        out_file.write(inkml_footer)
        out_file.close()

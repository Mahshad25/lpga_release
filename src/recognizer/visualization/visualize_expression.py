import networkx as nx
import matplotlib.pyplot as plt
from recognizer.graph_representation.Graph_construction import EDGE_ATTRIBUTES, NODE_ATTRIBUTES
from math import pi, sqrt
import numpy as np
from scipy.spatial.distance import cdist
from scipy.spatial import ConvexHull

class Visualization:


    def plot_trace_sets(self,nodes, node_trace_set, ax=plt, xwidth=1, ywidth=1):
        for node in nodes:
            current_stroke_set = node_trace_set[node]
            current_points = current_stroke_set.getPoints()
            # plot the points in the graph
            for point in current_points:
                if node == '3':
                    ax.plot(point[0] / xwidth, point[1] / ywidth, 'g.')
                elif node == '4':
                    ax.plot(point[0] / xwidth, point[1] / ywidth, 'b.')
                else:
                    ax.plot(point[0]/xwidth, point[1]/ywidth, 'k.')
            all_points = np.array(current_stroke_set.expression.getSymbolPoints(current_stroke_set.id))
            convexhullPoints = ConvexHull(all_points, qhull_options='QJ Pp').vertices
            #ax.plot(convexhullPoints[:,0]/xwidth, convexhullPoints[:,1]/ywidth, 'gx')
            eye = current_stroke_set.eye
            #ax.plot(eye[0]/xwidth, eye[1]/ywidth, 'ro')

    def plot_trace_Set(self, trace_Set, ax=plt,type='k.', xOffset=0, yOffset=0):
        points = trace_Set.getPoints()
        for p in points:
            ax.plot(p[0]-xOffset, p[1]-yOffset, type)


    def highlight_plot_points(self,stroke_set_eyes, ax=plt):
        for key, eye in stroke_set_eyes.items():
            ax.plot(eye[0], eye[1], 'ro')


    def draw_expression(self, expression, file_name):
        graph = expression.getLayoutGraph()
        node_trace_set = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET)
        self.plot_trace_sets(graph.nodes(), node_trace_set)
        plt.gca().invert_yaxis()
        plt.axis('scaled')
        plt.axis('off')
        plt.savefig(file_name + "_" + ".png")
        plt.clf()


    def graw_edges_graph(self, expression, file_name):
        graph = expression.getLayoutGraph()
        nodes = graph.nodes()
        node_trace_set = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET)
        self.plot_trace_sets(nodes, node_trace_set)

        for parent, child in graph.edges():
            eye_child = node_trace_set[child].eye
            eye_parent = node_trace_set[parent].eye
            parent_point = node_trace_set[parent].getPoints()[0]
            child_points = node_trace_set[child].getPoints()
            other_child_point = child_points[np.argmin(cdist(np.array([eye_parent]), child_points) )]

            if graph[parent][child][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] == 'NoPlot' or graph[child][parent][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] != 'NoRelation':
                continue

            if graph[parent][child][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] != 'NoRelation':
                plt.plot([eye_parent[0], eye_child[0]], [eye_parent[1], eye_child[1]], linestyle='-', color='green', linewidth=2.0, label=graph[parent][child][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION])
            else:
                plt.plot([eye_parent[0], eye_child[0]], [eye_parent[1], eye_child[1]], linestyle='--', color='red', linewidth=2.0, marker='>')


        plt.gca().invert_yaxis()
        plt.axis('scaled')
        plt.axis('off')
        plt.savefig(file_name + "_Complete" + ".png")
        plt.clf()



    def visualize_expression_graph(self, expression, file_name):

        graph = expression.getLayoutGraph()
        nodes = graph.nodes()
        node_trace_set = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET)
        minX, minY, maxX, maxY = expression.boundingBox
        midEx, midEy = (maxX+minX)/2, (maxY+minY)/2
        expWidth, expHeight = (maxX - minX), (maxY - minY)
        print(expHeight, expWidth)

        for node in nodes:
            currentNodeEdges = graph.edges(node)
            parent_node_id = node
            eye_parent = node_trace_set[parent_node_id].eye
            fig = plt.figure()
            rect =  [0, 0, 1,1]
            polar_rect = [(eye_parent[0]-midEx) / (expHeight), (-midEy+eye_parent[1]) / expWidth, 1,1]
            polar_rect = [0,0,1,1]
            ax_carthesian = fig.add_axes(rect)
            ax_polar = fig.add_axes(polar_rect, polar=True, frameon=False)
            ax_polar.grid(False)
            ax_polar.get_xaxis().set_visible(False)
            ax_polar.get_yaxis().set_visible(False)
            ax_polar.spines['polar'].set_visible(False)
            self.plot_trace_sets(nodes, node_trace_set, ax=ax_carthesian)

            for p,child_node_id in currentNodeEdges:
                radius=np.max(cdist(np.array([eye_parent]), expression.getSymbolPoints(node_trace_set[child_node_id].getId())))
                eye_child = node_trace_set[child_node_id].eye
                overlap=graph[node][child_node_id][EDGE_ATTRIBUTES.ANGULAR_RANGE_BLOCKED]
                theta=[]
                width=[]
                radii =[]
                minA, maxA, minB, maxB = overlap

                theta.append(minA*2*pi/360)
                radii.append(radius)
                width.append(2 * np.pi*maxA/360 -2*np.pi*minA/360)

                theta.append(minB * 2 * pi / 360)
                radii.append(radius)
                width.append(2 * np.pi * maxB / 360 - 2 * np.pi * maxB / 360)


                theta=np.array(theta)
                width = np.array(width)
                bars = ax_polar.bar(theta, radii, width=width)
                for r, bar in zip(radii, bars):
                    bar.set_facecolor(plt.cm.viridis(r/10))
                    bar.set_alpha(0.5)
                if overlap != []:
                    ax_carthesian.plot([eye_parent[0], eye_child[0]], [eye_parent[1], eye_child[1]])

                ax_carthesian.plot(eye_parent[0],eye_parent[1], 'go')

            ax_carthesian.invert_yaxis()
            #ax_carthesian.axis('scaled')
            #ax_carthesian.axis('off')
            ax_polar.invert_yaxis()
            plt.savefig(file_name+"_"+str(node)+".png")
            plt.clf()

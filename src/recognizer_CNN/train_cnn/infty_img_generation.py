# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 11:18:29 2018

@author: mxm7832
"""

import numpy as np
import matplotlib.pyplot as plt
from skimage import io, measure
from PIL import Image,ImageFilter
import sys                           
import os
def main():
    if len(sys.argv) < 3:
        exit()

    task_list = [str(task) for task in sys.argv[1].split(',')] #['symContext', 'symTarget', 'segContext', 'segTarget', 'parContext', 'parTarget']
    dataset= str(sys.argv[2]) #['test', 'train']

    lgPath='../../../Data/Expressions_v5/LG/'
    imgPath='../../../Data/Expressions_v5/IMG/'  
    
    test_List='../../../Data/infty/InftyCDB-2/Testing.txt'
    train_List='../../../Data/infty/InftyCDB-2/Training.txt'

    for task in task_list:
        outDir='cnn_data/'+task+'/'+dataset+'/'  
        outLabelDir='cnn_data/'+task+'/labels/'+dataset+'.txt'
        os.makedirs(outDir,exist_ok=True)
        os.makedirs('cnn_data/'+task+'/labels/',exist_ok=True)
        
    #listFile=train_List if dataset == 'train' else test_List
    dummy_List='../../../Data/infty/InftyCDB-2/small_Testing.txt'
    listFile=dummy_List
    
    #***************************generate target symbols**********************  
    if 'symTarget' in task_list:
        img_gen=ImgGenerator()    
        img_gen.symbol_dataset_generation(listFile,imgPath,lgPath,outDir,outLabelDir) 
    
    #***************************generate context for symbols**********************  
    if 'symContext' in task_list:
        img_gen=ImgGenerator(context=True,radiusFactor=4)    
        img_gen.symbol_dataset_generation_withContext(listFile,imgPath,lgPath,outDir,outLabelDir) 
       
    #***************************generate cc pairs for segmentation**********
    if 'segTarget' in task_list:
        txtPath='cnn_data/spatialFeat/segment/'+dataset+'/'
        img_gen=ImgGenerator()
        img_gen.segmentation_pair_generation(listFile,imgPath,lgPath,txtPath,outDir,outLabelDir)   
    
    #***************************generate context for segmentation pairs **********
    if 'segContext' in task_list:
        pairList='cnn_data/segTarget/labels/'+dataset+'.txt'
        img_gen=ImgGenerator(context=True,radiusFactor=0.75)
        img_gen.segmentation_pair_generation_Context(imgPath,lgPath,pairList,outDir)
    
    #***************************generate CCs ******************************  
    if 'CCs' in task_list:
        img_gen=ImgGenerator()
        img_gen.cc_generation(listFile,imgPath,lgPath,outDir)
    
    #*************************** generate pair images for parsing (us) ****************************** 
    if 'parTarget' in task_list:   
        txtPath='cnn_data/spatialFeat/parsing/'+dataset+'/'
        img_gen=ImgGenerator(context=True)
        img_gen.parsing_pair_generation(listFile,imgPath,lgPath,txtPath,outDir,outLabelDir)
    
    #*************************** generate pair images for parsing (us) + context ****************************** 
    if 'parContext'  in task_list:
        txtPath='cnn_data/spatialFeat/parsing/'+dataset+'/'
        img_gen=ImgGenerator(context=True,radiusFactor=0.5)
        img_gen.parsing_pair_generation_context(listFile,imgPath,lgPath,txtPath,outDir)
    
class ImgGenerator():
    
    def __init__(self,elseOnly=False,context=False,radiusFactor=0,balance=False,desiredSize=224):
        
        self.elseOnly=elseOnly
        self.context=context
        self.radiusFactor=radiusFactor
        self.balance=balance
        self.desiredSize=desiredSize

    
    @staticmethod    
    def combineComps(components, offsets):
        sizes = []
        for c in components:
            sizes.append(c.shape)
        sizes = np.array(sizes)
        #print(sizes)
        try:
            minx = np.amin(offsets[:,1])
            miny = np.amin(offsets[:,0])
            maxx = np.amax(offsets[:,1]+sizes[:,1])
            maxy = np.amax(offsets[:,0]+sizes[:,0])
        except:
            print(offsets)
            print(len(components))
            
        img = np.zeros([maxy-miny,maxx-minx])
        for i in range(len(components)):
            startx = offsets[i,1]-minx
            endx = startx + sizes[i,1]
            starty = offsets[i,0]-miny
            endy = starty + sizes[i,0]
            img[starty:endy,startx:endx] += components[i]
        
        return img, [miny, minx]        
      
    @staticmethod   
    def load_CCinfo(fileId,txtPath):
        pairLabels = {}
        txt = open(txtPath+fileId+'.txt')
        for line in txt:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            parent=parts[0]
            child=parts[1]
            pairLabels[parent,child]=parts[2]
        
        return pairLabels
    
    @staticmethod             
    def balance_samples(pairLabels,n_positive):
        import random
        balanced_pairLabels={} 
        # adding positive samples
        for pair,label in pairLabels.items():
            if int(label)==1:
                balanced_pairLabels[pair]=label
                del pairLabels[pair] #removing it from initial set
        # chosing the same number of negative samples randomely 
        for i in range(n_positive):
            key=random.choice(pairLabels.keys())
            balanced_pairLabels[key]=pairLabels[key]  
            del pairLabels[key]
        
        return balanced_pairLabels

    def extractComp(self,img, box):
        if self.context:
            factor=self.radiusFactor
            #chossing the larges dimensiona as radius
            if self.elseOnly:
                #remove the target and then extract context (to be more exact u can remove each CC instaed)
                img[box[0]:box[2],box[1]:box[3]]=0
            
            x=(box[2]-box[0])
            y=(box[3]-box[1])
            r=x if x>y else y
            diff=r*factor
            
            x_min=int(box[0]-diff) if box[0]>=diff else 0
            y_min=int(box[1]-diff) if box[1]>=diff else 0
            
            raw_comp = img[x_min:int(box[2]+diff),y_min:int(box[3]+diff)]
        
            return raw_comp
            
        raw_comp = img[box[0]:box[2]+1,box[1]:box[3]+1]
        labeled, numLabels = measure.label(raw_comp, return_num=True, connectivity=2, background=0)
        if numLabels == 1:
            return raw_comp
        else:
            trueLabel = -1
            for i in range(1, numLabels+1):
                if i in labeled[0,:] and i in labeled[-1,:] and i in labeled[:,0] and i in labeled[:,-1]:
                    #print (i)
                    trueLabel = i
                    break
            comp = np.array([[1 if labeled[y,x] == trueLabel else 0 for x in range(labeled.shape[1])] for y in range(labeled.shape[0])])
            return comp    
        
    def loadExpressionImages(self,fileId, boxes, imgsPath):
        imgs = {}
        #imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
        '''
        if self.context:
            for id,box in boxes.items():
                expImage = io.imread(imgsPath + fileId + ".PNG", as_grey=True).astype(bool)
                if len(box)!=4:
                    break
                imgs[id] = np.array(self.extractComp(expImage, box))        
            return imgs
        '''
        expImage = io.imread(imgsPath + fileId + ".PNG", as_grey=True).astype(bool)    
        for id,box in boxes.items():
            if len(box)!=4:
                #print('**********',id)
                continue
            imgs[id] = np.array(self.extractComp(expImage, box))
            
        return imgs
    
    @staticmethod
    def loadExpressionLabels(fileId, lgPath):
    
        symLabels, relLabels, comps, boxes ,sym_boxes= {}, {}, {}, {},{}
        lg = open(lgPath + fileId + ".lg")
        
        for line in lg:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            if parts[0].lower() == "o":
                symLabels[parts[1]] = parts[2]
                comps[parts[1]] = parts[4:]
            elif parts[0].lower() == "r":
                relLabels[(parts[1], parts[2])] = parts[3]
            elif parts[0].lower() == "#cc":
                boxes[parts[1]] = np.array(parts[2:]).astype(int)
            elif parts[0].lower() == "#sym": #MM: getting symbol bboxes instead of CC bboxes
                sym_boxes[parts[1]] = np.array(parts[2:]).astype(int)
                #print('box id', parts[1])
                #print('cc boxes', np.array(parts[2:]).astype(int))
        return symLabels, relLabels, comps, boxes,sym_boxes
    
    def img_resize(self,img):
        old_size = img.size  # old_size[0] is in (width, height) format
        desired_size=self.desiredSize
        ratio = float(desired_size)/max(old_size)
        new_size = tuple([int(x*ratio) for x in old_size])
        
        if min(new_size)==0:    
            new_size=tuple([1 if v==0 else v for v in new_size])       
        
        im = img.resize(new_size, Image.ANTIALIAS)
        # create a new image and paste the resized on it
        
        new_im = Image.new("RGB", (desired_size, desired_size))
        new_im.paste(im, ((desired_size-new_size[0])//2,
                            (desired_size-new_size[1])//2))
        return new_im
##################################### classification ################################ 
        
    def symbol_dataset_generation(self,listFile,imgPath,lgPath,outDir,outLabelDir):
        with open(outLabelDir, 'w') as f:
            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
        
                symLabels,_,comps,CCboxes,_=self.loadExpressionLabels(fileId, lgPath)
                ccimgs = self.loadExpressionImages(fileId, CCboxes, imgPath)
                for symId,label in symLabels.items():
                    
                    offsets = [CCboxes[ccId][:2] for ccId in comps[symId]] #minx and min y of all CCs of a symbol
                    components= [ccimgs[ccId] for ccId in comps[symId]] #list of CCimgs belong to a symbol
                    if len(components)==0:
                        print('no cc for this symbol')
                        break                
                    symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(symbolImg*255)             
                    im=im.convert("RGB")
                    #im = im.filter(ImageFilter.FIND_EDGES)
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+symId+'.PNG')
                    
                    labelLine=fileId+'_'+symId+', '+label+'\n'               
                    f.write(labelLine)
        f.close() 
    
    def symbol_dataset_generation_withContext(self,listFile,imgPath,lgPath,outDir,outLabelDir):
        with open(outLabelDir, 'w') as f:
         
            for fileid in open(listFile):
                print('FILE ID',fileid) 
                fileid=fileid.strip() 
                symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileid, lgPath)
                # list of images of symbols in one file
                symimgs=self.loadExpressionImages(fileid,sym_boxes,imgPath)
                
                for symId,img in symimgs.items():
                    im=(img*255).astype(np.uint8)
                    im=Image.fromarray(im) 
                    im=im.convert("RGB")
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileid+'_'+symId+'.PNG')
                    #im.filter(ImageFilter.GaussianBlur(16))
                    label=symLabels[symId]
                    labelLine=fileid+'_'+symId+', '+label+'\n'               
                    f.write(labelLine)
        f.close()
################################################ segmentation ########################################
        
    def segmentation_pair_generation(self,listFile,imgPath,lgPath,txtPath,outDir,outLabelDir):
        with open(outLabelDir, 'w') as f:
            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
        
                pairLabels=self.load_CCinfo(fileId,txtPath)
                positive_labels=[i for i in pairLabels.values() if int(i)==1] #listing all merge cases
                negative_labels=[i for i in pairLabels.values() if int(i)==0] #listing all split cases
                if self.balance and len(positive_labels)==0 :
                    continue
                elif self.balance and len(positive_labels)<len(negative_labels):
                    pairLabels=self.balance_samples(pairLabels,len(positive_labels))
                    
                _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, lgPath)
                ccimgs = self.loadExpressionImages(fileId, CCboxes, imgPath)
                
                for ccpairs,label in pairLabels.items():
                    
                    offsets = [CCboxes[ccId][:2] for ccId in ccpairs] #list of boxes belong to a symbol
                    components= [ccimgs[ccId] for ccId in ccpairs] #list of CCimgs belong to a symbol
                    if len(components)==0:
                        print('no cc for this symbol')
                        break                
                    symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(symbolImg*255)             
                    im=im.convert("RGB")
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+'.PNG')
                    
                    labelLine=fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+', '+label+'\n'               
                    f.write(labelLine)
        f.close()
        
    @staticmethod
    def readImgList(path):
        txt=open(path)
        reference={}
        fileIds={}
        for line in txt:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            fileId=parts[0].split('_')[0]
            parent=parts[0].split('_')[1]
            child=parts[0].split('_')[2]
            reference[parts[0]]=parts[1]
            if fileId in fileIds.keys():
                fileIds[fileId].append((parent,child))
            else:
                fileIds[fileId]=[(parent,child)]
        info={}
        for filename in fileIds.keys():
            pairLabels={}
            for pair in fileIds[filename]: 
                pairLabels[pair]=reference[filename+'_'+pair[0]+"_"+pair[1]]
            info[filename]=pairLabels
            
        return info
    
    @staticmethod
    def mergeBox(box1,box2):
        yMin=np.amin([box1[0],box2[0]])
        xMin=np.amin([box1[1],box2[1]])
        yMax=np.amax([box1[2],box2[2]])
        xMax=np.amax([box1[3],box2[3]])
        return [yMin,xMin,yMax,xMax]
    
    def segmentation_pair_generation_Context(self,imgPath,lgPath,txtPath,outDir):                                         
        #generate context for balanced train in train_224.txt
        file_info=self.readImgList(txtPath)
        for fileid,pairLabels in file_info.items():
            print('FILE ID',fileid) 
            #fileid=fileid.strip()        
            #pairLabels=self.load_CCinfo(fileid,txtPath)
            _,_,_,CCboxes,_=self.loadExpressionLabels(fileid, lgPath)
            #ccimgs = loadExpressionImages(fileid, CCboxes, imgPath,context=True)
            pair_bboxes={}
            for ccpairs,label in pairLabels.items():                    
                boxes = [CCboxes[ccId] for ccId in ccpairs]
                pair_bboxes[ccpairs[0]+'_'+ccpairs[1]]=self.mergeBox(boxes[0],boxes[1])
                
            pairImgs=self.loadExpressionImages(fileid,pair_bboxes,imgPath)#context flag on
            for pairId,img in pairImgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=im.convert("RGB")
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileid+'_'+pairId+'.PNG')
                #im.filter(ImageFilter.GaussianBlur(16))

    
    def cc_generation(self,listFile,imgPath,lgPath,outDir):
                                 
        #with open(outLabelDir, 'w') as f:    
        for fileId in open(listFile):
            fileId=fileId.strip()
            print ('fileID', fileId)
        
            #pairLabels=load_CCinfo(fileId,txtPath)                
            _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, lgPath)
            ccimgs = self.loadExpressionImages(fileId, CCboxes, imgPath)
            for ccId,img in ccimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileId+'_'+ccId+'.PNG')
                
########################################### Parsing ##########################################
                
    def parsing_pair_generation(self,listFile,imgPath,lgPath,txtPath,outDir,outLabelDir):
        with open(outLabelDir, 'w') as f:            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
        
                pairLabels=self.load_CCinfo(fileId,txtPath)

                symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileId, lgPath)
                # list of images of symbols in one file
                symimgs=self.loadExpressionImages(fileId,sym_boxes,imgPath)
                
                for ccpairs,label in pairLabels.items():
                    
                    offsets = [sym_boxes[ccId][:2] for ccId in ccpairs if ccId in symimgs.keys()] 
                    components= [symimgs[ccId] for ccId in ccpairs if ccId in symimgs.keys()] 
                    '''
                    if len(components)==0:
                        print('no cc for this symbol')
                        break
                    '''
                    pairImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(pairImg*255)             
                    im=im.convert("RGB")
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+'.PNG')
                    
                    labelLine=fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+', '+label+'\n'               
                    f.write(labelLine)
        f.close()

               
    def parsing_pair_generation_context(self,listFile,imgPath,lgPath,txtPath,outDir):
      
        for fileId in open(listFile):
            fileId=fileId.strip()
            print ('fileID', fileId)
    
            pairLabels=self.load_CCinfo(fileId,txtPath)

            symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileId, lgPath)
            # list of images of symbols in one file              
            pair_bboxes={}
            for ccpairs,label in pairLabels.items():                    
                boxes = [sym_boxes[ccId] for ccId in ccpairs ]
                if len(boxes[0])==4 and len(boxes[1])==4:
                    pair_bboxes[ccpairs[0]+'_'+ccpairs[1]]=self.mergeBox(boxes[0],boxes[1])
                else:
                    continue
            
            symimgs=self.loadExpressionImages(fileId,pair_bboxes,imgPath)
            for ccId,img in symimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileId+'_'+ccId+'.PNG')
        
if __name__ == '__main__':
    main()       

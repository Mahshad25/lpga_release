# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 11:18:29 2018

@author: mxm7832
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io, measure
from PIL import Image,ImageFilter
import shutil
                           

def generate_image_seg(config,los_pairs):
    targetDir,contextDir=make_output_dir(config)
    #generate target image
    img_gen_target=ImgGenerator(config)
    img_gen_target.segmentation_pair_generation(los_pairs,targetDir,testing=True)
    print("***** target images are generated *********")    
    #generate context image
    img_gen_context=ImgGenerator(config,context=True,radiusFactor=0.75)
    img_gen_context.segmentation_pair_generation_context(los_pairs,contextDir,testing=True)  
    print("***** context images are generated *********")
    
def generate_image_parsing(config,los_pairs,expressions):
    targetDir,contextDir=make_output_dir(config)
    #generate target image
    img_gen_target=ImgGenerator(config,context=True)
    img_gen_target.parsing_pair_generation(expressions,los_pairs,targetDir,testing=True)
    print("***** target images are generated *********")
    #generate context image    
    img_gen_context=ImgGenerator(config,context=True,radiusFactor=0.5)
    img_gen_context.parsing_pair_generation_context(expressions,los_pairs,contextDir,testing=True)
    print("***** context images are generated *********")
     
def generate_image_rec(config):
    targetDir,contextDir=make_output_dir(config)
    #generate target image
    img_gen_target=ImgGenerator(config)
    img_gen_target.symbol_dataset_generation(targetDir,testing=True)
    print("***** target images are generated *********")
    #generate context image    
    img_gen_context=ImgGenerator(config,context=True,radiusFactor=4)    
    img_gen_context.symbol_dataset_generation_withContext(contextDir,testing=True) 
    print("***** context images are generated *********")
    
def generate_image_rec_fromSeg(config,expressions):
    targetDir,contextDir=make_output_dir(config)
    #generate target image
    img_gen_target=ImgGenerator(config)
    img_gen_target.sym_imgs_from_segmentation(expressions,targetDir,testing=True)
    print("***** target images are generated *********")
    #generate context image    
    img_gen_context=ImgGenerator(config,context=True,radiusFactor=4)    
    img_gen_context.sym_imgs_withContext(expressions,contextDir,testing=True) 
    print("***** context images are generated *********") 
    
def make_output_dir(config):
    outDir=config.get_str("CNN_IMAGE_OUTPUT_DIR")
    targetDir=outDir+'target/'
    contextDir=outDir+'context/'

    if not os.path.exists(outDir):
        os.makedirs(outDir)
        os.mkdir(targetDir)
        os.mkdir(contextDir)
    else:
        shutil.rmtree(outDir)#removes all the subdirectories!
        os.makedirs(outDir)    
        os.mkdir(targetDir) 
        os.mkdir(contextDir)
        
    return targetDir,contextDir 
       
class ImgGenerator():
    
    def __init__(self,config,elseOnly=False,context=False,radiusFactor=0,balance=False):
        
        self.elseOnly=elseOnly
        self.context=context
        self.radiusFactor=radiusFactor
        self.balance=balance
        self.test_fileList = config.get_str("TESTING_DATASET_PATH")
        self.train_fileList=config.get_str("TRAINING_DATASET_PATH")
        
        self.desiredSize=config.get_int("CNN_IMAGE_SIZE",224)
        
        self.imgPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
        self.lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
        #self.outDir = config.get_str("CNN_IMAGE_OUTPUT_DIR")
        self.outLabelDir = config.get_str("CNN_IMAGE_LABEL_DIR")
        
    
    @staticmethod    
    def combineComps(components, offsets):
        sizes = []
        for c in components:
            sizes.append(c.shape)
        sizes = np.array(sizes)
        #print(sizes)
        try:
            minx = int(np.amin(offsets[:,1]))
            miny = int(np.amin(offsets[:,0]))
            maxx = int(np.amax(offsets[:,1]+sizes[:,1]))
            maxy = int(np.amax(offsets[:,0]+sizes[:,0]))
        except:
            print(offsets)
            print(len(components))

        img = np.zeros([maxy-miny,maxx-minx])
        for i in range(len(components)):
            startx = int(offsets[i,1]-minx)
            endx = int(startx + sizes[i,1])
            starty = int(offsets[i,0]-miny)
            endy = int(starty + sizes[i,0])
            img[starty:endy,startx:endx] += components[i]
        
        return img, [miny, minx]        
      
    @staticmethod   
    def load_CCinfo(fileId,txtPath):
        pairLabels = {}
        txt = open(txtPath+fileId+'.txt')
        for line in txt:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            parent=parts[0]
            child=parts[1]
            pairLabels[parent,child]=parts[2]
        
        return pairLabels
    
    @staticmethod             
    def balance_samples(pairLabels,n_positive):
        import random
        balanced_pairLabels={} 
        # adding positive samples
        for pair,label in pairLabels.items():
            if int(label)==1:
                balanced_pairLabels[pair]=label
                del pairLabels[pair] #removing it from initial set
        # chosing the same number of negative samples randomely 
        for i in range(n_positive):
            key=random.choice(pairLabels.keys())
            balanced_pairLabels[key]=pairLabels[key]  
            del pairLabels[key]
        
        return balanced_pairLabels

    def extractComp(self,img, box):
        if self.context:
            factor=self.radiusFactor
            #chossing the larges dimensiona as radius
            if self.elseOnly:
                #remove the target and then extract context (to be more exact u can remove each CC instaed)
                img[box[0]:box[2],box[1]:box[3]]=0
            
            x=(box[2]-box[0])
            y=(box[3]-box[1])
            r=x if x>y else y
            diff=r*factor
            
            x_min=int(box[0]-diff) if box[0]>=diff else 0
            y_min=int(box[1]-diff) if box[1]>=diff else 0
            
            raw_comp = img[x_min:int(box[2]+diff),y_min:int(box[3]+diff)]
        
            return raw_comp
            
        raw_comp = img[box[0]:box[2]+1,box[1]:box[3]+1]
        labeled, numLabels = measure.label(raw_comp, return_num=True, connectivity=2, background=0)
        if numLabels == 1:
            return raw_comp
        else:
            trueLabel = -1
            for i in range(1, numLabels+1):
                if i in labeled[0,:] and i in labeled[-1,:] and i in labeled[:,0] and i in labeled[:,-1]:
                    #print (i)
                    trueLabel = i
                    break
            comp = np.array([[1 if labeled[y,x] == trueLabel else 0 for x in range(labeled.shape[1])] for y in range(labeled.shape[0])])
            return comp    
        
    def loadExpressionImages(self,fileId, boxes, imgsPath):
        imgs = {}
        #imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
        '''
        if self.context:
            for id,box in boxes.items():
                expImage = io.imread(imgsPath + fileId + ".PNG", as_grey=True).astype(bool)
                if len(box)!=4:
                    break
                imgs[id] = np.array(self.extractComp(expImage, box))        
            return imgs
        '''
        expImage = io.imread(imgsPath + fileId + ".PNG", as_gray=True).astype(bool)    
        for id,box in boxes.items():
            if len(box)!=4:
                #print('**********',id)
                continue
            imgs[id] = np.array(self.extractComp(expImage, box))
            
        return imgs
    
    @staticmethod
    def loadExpressionLabels(fileId, lgPath):
    
        symLabels, relLabels, comps, boxes ,sym_boxes= {}, {}, {}, {},{}
        lg = open(lgPath + fileId + ".lg")
        
        for line in lg:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            if parts[0].lower() == "o":
                symLabels[parts[1]] = parts[2]
                comps[parts[1]] = parts[4:]
            elif parts[0].lower() == "r":
                relLabels[(parts[1], parts[2])] = parts[3]
            elif parts[0].lower() == "#cc":
                boxes[parts[1]] = np.array(parts[2:]).astype(int)
            elif parts[0].lower() == "#sym": #MM: getting symbol bboxes instead of CC bboxes
                sym_boxes[parts[1]] = np.array(parts[2:]).astype(int)
                #print('box id', parts[1])
                #print('cc boxes', np.array(parts[2:]).astype(int))
        return symLabels, relLabels, comps, boxes,sym_boxes
    
    def img_resize(self,img):
        old_size = img.size  # old_size[0] is in (width, height) format
        desired_size=self.desiredSize
        ratio = float(desired_size)/max(old_size)
        new_size = tuple([int(x*ratio) for x in old_size])
        
        if min(new_size)==0:    
            new_size=tuple([1 if v==0 else v for v in new_size])       
        
        im = img.resize(new_size, Image.ANTIALIAS)
        # create a new image and paste the resized on it
        
        new_im = Image.new("RGB", (desired_size, desired_size))
        new_im.paste(im, ((desired_size-new_size[0])//2,
                            (desired_size-new_size[1])//2))
        return new_im
##################################### classification ################################ 
        
    def symbol_dataset_generation(self,outDir,testing=True):        
        listFile=self.test_fileList if testing else  self.train_fileList
        
        with open(self.outLabelDir, 'w') as f:
            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
        
                symLabels,_,comps,CCboxes,_=self.loadExpressionLabels(fileId, self.lgPath)
                ccimgs = self.loadExpressionImages(fileId, CCboxes, self.imgPath)
                for symId,label in symLabels.items():
                    
                    offsets = [CCboxes[ccId][:2] for ccId in comps[symId]] #minx and min y of all CCs of a symbol
                    components= [ccimgs[ccId] for ccId in comps[symId]] #list of CCimgs belong to a symbol
                    if len(components)==0:
                        print('no cc for this symbol')
                        break                
                    symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(symbolImg*255)             
                    im=im.convert("RGB")
                    #im = im.filter(ImageFilter.FIND_EDGES)
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+symId+'.PNG')
                    
                    labelLine=fileId+'_'+symId+', '+label+'\n'               
                    f.write(labelLine)
        f.close() 
    
    def symbol_dataset_generation_withContext(self,outDir,testing=True):
        listFile=self.test_fileList if testing else  self.train_fileList
    
        for fileid in open(listFile):
            print('fileID',fileid) 
            fileid=fileid.strip() 
            symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileid, self.lgPath)
            # list of images of symbols in one file
            symimgs=self.loadExpressionImages(fileid,sym_boxes,self.imgPath)
            
            for symId,img in symimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=im.convert("RGB")
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileid+'_'+symId+'.PNG')
                #im.filter(ImageFilter.GaussianBlur(16))

        
    def sym_imgs_from_segmentation(self,expressions,outDir,testing=True):        
        listFile=self.test_fileList if testing else  self.train_fileList
        
        with open(self.outLabelDir, 'w') as f:
            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
                #get the CC boxes from lg files
                _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, self.lgPath)
                #get the compMap, sym labels and symboxes from expression
                #print(expressions.keys())
                symLabels,_, comps,_=self.loadExpressionInfo(expressions[fileId])

                ccimgs = self.loadExpressionImages(fileId, CCboxes, self.imgPath)
                
                if symLabels:
                    for symId,label in symLabels.items():
                    
                        offsets = [CCboxes[ccId][:2] for ccId in comps[symId]] #minx and min y of all CCs of a symbol
                        components= [ccimgs[ccId] for ccId in comps[symId]] #list of CCimgs belong to a symbol
                        if len(components)==0:
                            print('no cc for this symbol')
                            break                
                        symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                        im=Image.fromarray(symbolImg*255)             
                        im=im.convert("RGB")
                        #im = im.filter(ImageFilter.FIND_EDGES)
                        im=self.img_resize(im)
                        #plt.imshow(im)
                        im.save(outDir+fileId+'_'+symId+'.PNG')
                        
                        labelLine=fileId+'_'+symId+', '+label+'\n'               
                        f.write(labelLine)
                
                #when there is no symbol ground truth available
                else:
                    for symId,comp in comps.items():
                        
                        offsets = [CCboxes[ccId][:2] for ccId in comp] #minx and min y of all CCs of a symbol
                        components= [ccimgs[ccId] for ccId in comp] #list of CCimgs belong to a symbol
                        if len(components)==0:
                            print('no cc for this symbol')
                            break                
                        symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                        im=Image.fromarray(symbolImg*255)             
                        im=im.convert("RGB")
                        #im = im.filter(ImageFilter.FIND_EDGES)
                        im=self.img_resize(im)
                        #plt.imshow(im)
                        im.save(outDir+fileId+'_'+symId+'.PNG')
                        labelLine=fileId+'_'+symId+'\n'               
                        f.write(labelLine)
    
        f.close()

    def sym_imgs_withContext(self,expressions,outDir,testing=True):
        listFile=self.test_fileList if testing else  self.train_fileList
    
        for fileid in open(listFile):
            print('fileID',fileid) 
            fileid=fileid.strip() 
            symLabels,_,_,sym_boxes = self.loadExpressionInfo(expressions[fileid])
            # list of images of symbols in one file
            symimgs=self.loadExpressionImages(fileid,sym_boxes,self.imgPath)
            
            for symId,img in symimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=im.convert("RGB")
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileid+'_'+symId+'.PNG')
                #im.filter(ImageFilter.GaussianBlur(16))
        
    @staticmethod
    def loadExpressionInfo(expr):
        symLabels=expr.symbolLabels
        relLabels=expr.relationLabels
        comps={}
        sym_boxes={}
        for node in expr.expressionGraph.nodes():
            bbox=expr.componentMap[node].original_box
            sym_boxes[node]=np.array([round(bbox[2]),round(bbox[0]),round(bbox[3]),round(bbox[1])])
            comps[node]= expr.componentMap[node].trace_list         
        return symLabels, relLabels, comps,sym_boxes
                        
################################################ segmentation ########################################
        
    def segmentation_pair_generation(self,los_pairs,outDir,testing=True):        
        listFile=self.test_fileList if testing else  self.train_fileList
        
        with open(self.outLabelDir, 'w') as f:
            
            for fileId in open(listFile):
                fileId=fileId.strip()
                print ('fileID', fileId)
                
                pairLabels=los_pairs[fileId]                
                #pairLabels=self.load_CCinfo(fileId,txtPath)
                positive_labels=[i for i in pairLabels.values() if int(i)==1] #listing all merge cases
                negative_labels=[i for i in pairLabels.values() if int(i)==0] #listing all split cases
                if self.balance and len(positive_labels)==0 :
                    continue
                elif self.balance and len(positive_labels)<len(negative_labels):
                    pairLabels=self.balance_samples(pairLabels,len(positive_labels))                    
                _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, self.lgPath)
                ccimgs = self.loadExpressionImages(fileId, CCboxes, self.imgPath)
                
                for ccpairs,label in pairLabels.items():
                    
                    offsets = [CCboxes[ccId][:2] for ccId in ccpairs] #list of boxes belong to a symbol
                    components= [ccimgs[ccId] for ccId in ccpairs] #list of CCimgs belong to a symbol
                    if len(components)==0:
                        print('no cc for this symbol')
                        break                
                    symbolImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(symbolImg*255)             
                    im=im.convert("RGB")
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+'.PNG')
                    
                    labelLine=fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+', '+str(label)+'\n'               
                    f.write(labelLine)
        f.close()

    def segmentation_pair_generation_context(self,los_pairs,outDir,testing=True):        
        listFile=self.test_fileList if testing else  self.train_fileList
        
        #generate context for balanced train in train_224.txt
        #file_info=self.readImgList(txtPath)
        #for fileid,pairLabels in file_info.items():
        for fileId in open(listFile):
            fileId=fileId.strip()
            print('FILE ID',fileId) 
            #fileid=fileid.strip()        
            #pairLabels=self.load_CCinfo(fileid,txtPath)
            _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, self.lgPath)
            #ccimgs = loadExpressionImages(fileid, CCboxes, imgPath,context=True)
            pairLabels=los_pairs[fileId]                
            pair_bboxes={}
            for ccpairs,label in pairLabels.items():                    
                boxes = [CCboxes[ccId] for ccId in ccpairs]
                pair_bboxes[ccpairs[0]+'_'+ccpairs[1]]=self.mergeBox(boxes[0],boxes[1])
                
            pairImgs=self.loadExpressionImages(fileId,pair_bboxes,self.imgPath)#context flag on
            for pairId,img in pairImgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=im.convert("RGB")
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileId+'_'+pairId+'.PNG')
                #im.filter(ImageFilter.GaussianBlur(16))

    
    def cc_generation(self,listFile,imgPath,lgPath,outDir):
                                 
        #with open(outLabelDir, 'w') as f:    
        for fileId in open(listFile):
            fileId=fileId.strip()
            print ('fileID', fileId)
        
            #pairLabels=load_CCinfo(fileId,txtPath)                
            _,_,_,CCboxes,_=self.loadExpressionLabels(fileId, lgPath)
            ccimgs = self.loadExpressionImages(fileId, CCboxes, imgPath)
            for ccId,img in ccimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileId+'_'+ccId+'.PNG')
        
    @staticmethod
    def readImgList(path):
        txt=open(path)
        reference={}
        fileIds={}
        for line in txt:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            fileId=parts[0].split('_')[0]
            parent=parts[0].split('_')[1]
            child=parts[0].split('_')[2]
            reference[parts[0]]=parts[1]
            if fileId in fileIds.keys():
                fileIds[fileId].append((parent,child))
            else:
                fileIds[fileId]=[(parent,child)]
        info={}
        for filename in fileIds.keys():
            pairLabels={}
            for pair in fileIds[filename]: 
                pairLabels[pair]=reference[filename+'_'+pair[0]+"_"+pair[1]]
            info[filename]=pairLabels
            
        return info
    
    @staticmethod
    def mergeBox(box1,box2):
        yMin=np.amin([box1[0],box2[0]])
        xMin=np.amin([box1[1],box2[1]])
        yMax=np.amax([box1[2],box2[2]])
        xMax=np.amax([box1[3],box2[3]])
        return [yMin,xMin,yMax,xMax]
    
                
########################################### Parsing ##########################################
                
    def parsing_pair_generation(self, expressions,los_pairs,outDir,testing=True):
        fileList=self.test_fileList if testing else  self.train_fileList
        # a dictionary containing a dictionary of pairs for each file
        with open(self.outLabelDir, 'w') as f:            
            for fileId in open(fileList):
                fileId=fileId.strip()
                print ('fileID', fileId)
                #MM:changing to get from expressions
                symLabels, _, _,sym_boxes = self.loadExpressionInfo(expressions[fileId])
                #symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileId, self.lgPath)
                # list of images of symbols in one file
                symimgs=self.loadExpressionImages(fileId,sym_boxes,self.imgPath)
        
                #pairLabels=self.load_CCinfo(fileId,txtPath)
                pairLabels=los_pairs[fileId]                
                for ccpairs,label in pairLabels.items():
                    
                    offsets = [sym_boxes[ccId][:2] for ccId in ccpairs if ccId in symimgs.keys()] 
                    components= [symimgs[ccId] for ccId in ccpairs if ccId in symimgs.keys()] 
                    '''
                    if len(components)==0:
                        print('no cc for this symbol')
                        break
                    '''
                    pairImg, offset = self.combineComps(components,np.array(offsets)) 
                    im=Image.fromarray(pairImg*255)             
                    im=im.convert("RGB")
                    im=self.img_resize(im)
                    #plt.imshow(im)
                    im.save(outDir+fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+'.PNG')
                    
                    labelLine=fileId+'_'+ccpairs[0]+'_'+ccpairs[1]+', '+str(label)+'\n'               
                    f.write(labelLine)
        f.close()

               
    def parsing_pair_generation_context(self,expressions, los_pairs,outDir,testing=True):
        fileList=self.test_fileList if testing else  self.train_fileList
        for fileId in open(fileList):
            fileId=fileId.strip()
            print ('fileID', fileId)
            symLabels, _, _,sym_boxes = self.loadExpressionInfo(expressions[fileId])
            #symLabels, _, _,_,sym_boxes = self.loadExpressionLabels(fileId, self.lgPath)
            # list of images of symbols in one file 
        
            #pairLabels=self.load_CCinfo(fileId,txtPath)
            pairLabels=los_pairs[fileId]                             
            pair_bboxes={}
            for ccpairs,label in pairLabels.items():                    
                boxes = [sym_boxes[ccId] for ccId in ccpairs ]
                if len(boxes[0])==4 and len(boxes[1])==4:
                    pair_bboxes[ccpairs[0]+'_'+ccpairs[1]]=self.mergeBox(boxes[0],boxes[1])
                else:
                    continue
            
            symimgs=self.loadExpressionImages(fileId,pair_bboxes,self.imgPath)
            for ccId,img in symimgs.items():
                im=(img*255).astype(np.uint8)
                im=Image.fromarray(im) 
                im=self.img_resize(im)
                #plt.imshow(im)
                im.save(outDir+fileId+'_'+ccId+'.PNG')
